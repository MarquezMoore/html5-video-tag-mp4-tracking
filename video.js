
let videos = document.getElementsByTagName('video');
let allVideos = {};

for (let i = 0; i < videos.length; i++){
         // Retrive id's of each video
    let currentVid = videos[i];
    let videoID = videos[i].getAttribute('id');
       
// Declare propeties for allVideos object
    // Add object to allVideos with property name of current video if
    allVideos[videoID] = {};
    // Add highest percentage watch to limit execution
    allVideos[videoID].highestPercentage = 0;
    // Add properties to the nested videoID object
    allVideos[videoID].videoSource = currentVid.currentSrc;
    currentVid.oncanplay= function (){
        allVideos[videoID].videoDuration = currentVid.duration;
    }
    // Add Event Listeners
    videos[i].addEventListener('play', eventHandler);
    videos[i].addEventListener('pause', function(currentVid){
            // prevent pause event from firing on complete 
            if(currentVid.currentTime < currentVid.duration){
                eventHandler(currentVid);
            } else{
                return;
            }
    });
    videos[i].addEventListener('ended', eventHandler);
    videos[i].addEventListener('timeupdate', eventHandler);
};

function eventHandler(currentVid){
    
    // console.log(currentVid);

    switch(currentVid.type){
        case 'timeupdate':
            // grab the current time at each 
            allVideos[currentVid.target.id].currentTime = currentVid.target.currentTime;
            
            // calculate the precentage of the video watch ontil now
            let percentage = Math.round((100 * (allVideos[currentVid.target.id].currentTime / allVideos[currentVid.target.id].videoDuration)));
            
            if((percentage % 10 === 0 && percentage > 0)){
                // set percentageWatched property for the respective video
                allVideos[currentVid.target.id].percentageWatched = percentage;
                console.log(percentage);
                // Checked to ensure that highest percentage is updated if not already
            } 
            break;
        case 'play':
            console.log(allVideos);
            break;
        case 'pause':
            console.log('Pause');
            break;
        case 'ended':
            console.log('completed');
            break;
    }
}








